User-agent: *
Allow: /*.js
Allow: /*.css

Host: https://www.twinsann.com/
Sitemap: https://www.twinsann.com/sitemap.xml