// export const API_URL = process.env.API_URL || 'http://0.0.0.0:3000';
// export const API_URL = process.env.API_URL || 'http://localhost:3000';
export const API_URL = process.env.API_URL || 'https://twinsann.com';
export const PHOTO_STORAGE_URL = process.env.NEXT_PUBLIC_PHOTO_STORAGE_URL;
export const API_IMG_URL = API_URL + 'shared-components/src/itemImages/';